<?php

declare(strict_types=1);

namespace Application;

/**
 * Class Application
 *
 * @package Application
 */
class Application
{
    /**
     * Run app.
     */
    public static function run()
    {
        static::loadDependence();
    }

    /**
     * Load the app dependence.
     */
    protected static function loadDependence()
    {
        // todo
    }
}