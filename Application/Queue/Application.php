<?php

declare(strict_types=1);

namespace Application\Queue;

use Swoole\Coroutine;
use Swoole\Coroutine\Scheduler;

/**
 * Class Application
 *
 * @package Application\Queue
 */
final class Application extends \Application\Application
{
    /**
     * Start register
     */
    protected static function loadDependence()
    {
        parent::loadDependence();

        $server = new Server('0.0.0.0', 9501);
        $server->start();
    }

    /**
     * Send messages quickly.
     *
     * @param $queue
     * @param $message
     * @return bool
     */
    public static function quickSendMessage($queue, $message): bool
    {
        $scheduler = new Scheduler();

        $options = Coroutine::getOptions();

        if (!isset($options['hook_flags'])) {
            $scheduler->set(['hook_flags' => SWOOLE_HOOK_ALL]);
        }

        $scheduler->add(function ($queue, $message) {
            $client = new Client('127.0.0.1', 9501);
            $client->dispatch($message, $queue, function () {
                var_dump('failure');
            });
            $client->close();
        }, $queue, $message);

        return $scheduler->start();
    }
}

