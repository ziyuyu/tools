<?php

declare(strict_types = 1);

namespace Application\Queue;

/**
 * Class Message
 *
 * @package Application\Queue
 */
class Message
{
    /** @var string */
    private string $queue;

    /** @var string */
    private string $message;

    /**
     * Message constructor.
     *
     * @param string $queue
     * @param string $message
     */
    public function __construct(string $queue, string $message)
    {
        $this->queue = $queue;
        $this->message = $message;
    }

    /**
     * Convert to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return <<< data
{"queue": "{$this->queue}", "message":"{$this->message}"}
data;
    }

    /**
     * Data analysis.
     *
     * @param string $message
     * @return static
     * @throws \Exception
     */
    public static function parseMessage(string $message): static
    {
        $data = json_decode($message, true);
        if (isset($data['queue']) && isset($data['message'])) {
            return new static($data['message'], $data['message']);
        }

        throw new \Exception('Data parsing failed.');
    }

    /**
     * @return string
     */
    public function getQueue(): string
    {
        return $this->queue;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}