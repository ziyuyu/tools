<?php

namespace Test;

/**
 * Class TestClient
 * @package Test
 */
class TestClient extends \Grpc\BaseStub
{
    /**
     * TestClient constructor.
     * @param $hostname
     * @param $opts
     * @param null $channel
     */
    public function __construct($hostname, $opts, $channel = null)
    {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param TestRequest $argument
     * @param array $metadata
     * @param array $options
     * @return mixed
     */
    public function run(TestRequest $argument, array $metadata = [], array $options = [])
    {
        return $this->_simpleRequest('/Test.Greeter/run', $argument, ['\Test\TestReply', 'decode'], $metadata, $options);
    }
}