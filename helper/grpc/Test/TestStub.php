<?php

namespace Test;

/**
 * Class TestStub
 * @package Test
 */
class TestStub
{
    /**
     * @param TestRequest $request
     * @param \Grpc\ServerContext $context
     * @return TestReply|null
     */
    public function run(TestRequest $request, \Grpc\ServerContext $context): ?TestReply
    {
        $context->setStatus(\Grpc\status::unimplemented());

        return null;
    }

    /**
     * @return \Grpc\MethodDescriptor[]
     */
    public final function getMethodDescriptors(): array
    {
        return [
            '/Test.Greeter/run' => new \Grpc\MethodDescriptor(
                $this,
                'run',
                TestRequest::class,
                \Grpc\MethodDescriptor::UNARY_CALL
            ),
        ];
    }
}