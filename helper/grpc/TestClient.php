<?php

require __DIR__ . '/../../vendor/autoload.php';

$client = new \Test\TestClient('127.0.0.1:8000', [
    'credentials' => \Grpc\ChannelCredentials::createInsecure(),
]);

$request = new \Test\TestRequest();
$request->setName('ziyuyu');

list($response, $status) = $client->run($request)->wait();

if (\Grpc\STATUS_OK != $status->code) {
    echo "Error code: {$status->code} - message: {$status->details}" . PHP_EOL;
}

echo $response->getMessage() . PHP_EOL;