<?php

require __DIR__ . '/../../vendor/autoload.php';

/**
 * Class Test
 */
class Test extends \Test\TestStub
{
    /**
     * @param \Test\TestRequest $request
     * @param \Grpc\ServerContext $context
     * @return \Test\TestReply|null
     */
    public function run(\Test\TestRequest $request, \Grpc\ServerContext $context): ?\Test\TestReply
    {
        $name = $request->getName();

        $response  = new \Test\TestReply();
        $response->setMessage("message: {$name}");

        return $response;
    }
}

$server = new \Grpc\RpcServer();
$server->addHttp2Port('0.0.0.0:8000');
$server->handle(new Test());
$server->run();