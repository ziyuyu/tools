<?php

require __DIR__ . '/../../vendor/autoload.php';


$connection = new \PhpAmqpLib\Connection\AMQPStreamConnection('127.0.0.1', 5672, 'guest', 'guest', '/');

if(!$connection){
    echo "连接失败";
    exit();
}

/*声明一个通道*/
$channel = $connection->channel();

/*声明一个队列*/
$channel->queue_declare('order_satistic_queue', false, true, false, false);

/*每次消费限流1000*/
$channel->basic_qos(null, 1000, null);

/*开启消费监听*/
$channel->basic_consume('order_satistic_queue', '', false, false, false, false, function ($message)
{
    echo "\n--------\n";
    echo $message->body;
    echo "\n--------\n";

    /*从rabbitmq里面取出json数据*/
    $data_json = $message->body;

    /*把json数据编译后，放入$data变量，
    $data变量是个二维数组，代表多条mysql数据*/
    $data = json_decode($data_json, true);


    /*数据保存到mysql数据库*/
//    saveToMysql($data, $message);
});


while ($channel->is_consuming()) {
    // 测试时10秒后让进程死掉，正式环境这里第三个参数要填0而不是10
    $channel->wait(null, false, 0);
}