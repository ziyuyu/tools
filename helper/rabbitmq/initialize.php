<?php

$connection = new \AMQPConnection([
    'host' => '127.0.0.1',
    'vhost' => '/',
    'port' => 5672,
    'login' => 'guest',
    'password' => 'guest',
    'read_timeout' => 2,
    'connect_timeout' => 3,
    'write_timeout' => 3
]);

if (! $connection->connect()) {
    throw new \Exception('Could not connect');
}

var_dump($connection->getLogin());
var_dump($connection->isConnected());

if (! $connection->disconnect()) {
    throw new \Exception('Could not disconnect');
}
