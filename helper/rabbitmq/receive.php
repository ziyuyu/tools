<?php

require __DIR__ . '/../../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$queue_name = 'queue-001';
$exchange_name = 'exchange-001';
$exchange_type = 'direct';
$bind_key = 'key-001';

$connection = new AMQPStreamConnection('101.34.12.64', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->exchange_declare($exchange_name, $exchange_type, false, false, false);
[$queue_name, ,] = $channel->queue_declare('', false, false, true, false);
$channel->queue_bind($queue_name, $exchange_name, $bind_key);

$callback = function ($message) {
    echo '[*] ---> Received: ' . PHP_EOL;
    var_dump($message->body);
    sleep(substr_count($message->body, '.'));

    $message->ack();

    echo '[*] ---> Done.' . PHP_EOL;
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume($queue_name, '', false, true, false, false, $callback);

while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();
