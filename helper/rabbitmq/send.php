<?php

require __DIR__ . '/../../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$queue_name = 'queue-001';
$exchange_name = 'exchange-001';
$exchange_type = 'direct';
$bind_key = 'key-002';

$connection = new AMQPStreamConnection('101.34.12.64', 5672, 'guest', 'guest');
$channel = $connection->channel();

// direct topic headers fanout
$channel->exchange_declare($exchange_name, $exchange_type, false, false, false);

$channel->set_ack_handler(function () {});
$channel->set_nack_handler(function () {});
// 消息不可达目的时回退，例如不可路由时 如果设置了备份交换机，那么备份交换机优先级高
$channel->set_return_listener(function () {});
$channel->confirm_select();
$data = implode(' ', array_slice($argv, 1));
if (empty($data)) {
    $data = 'Hello World!';
}
$message = new AMQPMessage($data, [
//    'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
]);
$channel->basic_publish($message, $exchange_name, $bind_key);

$channel->wait_for_pending_acks();

$channel->wait_for_pending_acks_returns();

$channel->close();
$connection->close();
