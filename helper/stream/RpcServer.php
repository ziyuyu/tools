<?php

require __DIR__ . '/../../vendor/autoload.php';

$rpcServer = new \Tools\Rpc\Stream\Server('0.0.0.0', 8000);
$rpcServer->registerService(new TestService());
$rpcServer->run();


class TestService
{
    public function test($sss)
    {
        return __METHOD__ . $sss;
    }

    public static function testStatic()
    {
        return __METHOD__;
    }

    protected function testProtected()
    {
        return __METHOD__;
    }

    protected static function testProtectedStatic()
    {
        return __METHOD__;
    }

    private function testPrivate()
    {
        return __METHOD__;
    }

    private static function testPrivateStatic()
    {
        return __METHOD__;
    }
}

