<?php

require __DIR__ . '/../../vendor/autoload.php';

use Swoole\Coroutine;
use Swoole\Coroutine\Scheduler;
use Swoole\Coroutine\Http\Client;

$scheduler = new Scheduler();
$options = Coroutine::getOptions();
if (!isset($options['hook_flags'])) {
    $scheduler->set(['hook_flags' => SWOOLE_HOOK_ALL]);
}
$scheduler->add(function () {
    $client = new Client('127.0.0.1', 9501);
    if ($client->upgrade('/')) {
        Coroutine::create(function () use ($client) {
            var_dump($client->recv());
        });
    }

    Coroutine::create(function () use ($client) {
        $standardInput = fopen('php://stdin', 'r');
        $standardOutput = fopen('php://stdout', 'w');

        while (true) {
            fputs(STDOUT, '* Please input content to send server: ');
            fputs(STDERR, '* Please input content to send server: ');
            $content = strtolower(trim(fgets(STDIN)));

            $client->push($content);
        }
    });
});
$scheduler->start();

