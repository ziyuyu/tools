<?php

$client = new Yar_client('http://101.34.12.64:8000/helper/operator.php');
if (! $client->setOpt(YAR_OPT_PACKAGER, YAR_PACKAGER_JSON)) {
    throw new \Exception();
}
if (! $client->setOpt(YAR_OPT_PERSISTENT, true)) {
    throw new \Exception();
}
if (! $client->setOpt(YAR_OPT_CONNECT_TIMEOUT, 2000)) {
    throw new \Exception();
}
if (! $client->setOpt(YAR_OPT_TIMEOUT, 5000)) {
    throw new \Exception();
}

var_dump($client->add(1, 2));

var_dump($client->call('empty', [2, 3]));