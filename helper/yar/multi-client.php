<?php

function callback($result, $callInformation) {
    var_dump($result, $callInformation);
}

$operator = 'http://101.34.12.64:8000/helper/operator.php';

Yar_Concurrent_Client::call($operator, 'add', [1, 2], 'callback');
//Yar_Concurrent_Client::call($operator, 'sub', [1, 2], 'callback');
//Yar_Concurrent_Client::call($operator, 'mul', [1, 2], 'callback');
//Yar_Concurrent_Client::call($operator, 'div', [1, 2], 'callback');

Yar_Concurrent_Client::loop();

Yar_Concurrent_Client::reset();
Yar_Concurrent_Client::call($operator, 'div', [1, 2], 'callback');

Yar_Concurrent_Client::loop();