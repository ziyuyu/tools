<?php

$server = new Yar_Server(new Operator());
if (! $server->handle()) {
    throw new \Exception('Error!');
}

class Operator
{
    public function add($a, $b)
    {
        return $a + $b;
    }

    public function sub($a, $b)
    {
        return $a - $b;
    }

    public function mul($a, $b)
    {
        return $a * $b;
    }

    public function div($a, $b)
    {
        return $a / $b;
    }

    public function empty()
    {
        return ;

    }
}
