<?php

declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

\Tools\Helper\AutoRestart::handle(['../'],
    new \Tools\DataStructure\Closure(function () {
        echo date('Y-m-d H:i:s') . PHP_EOL;
    })
);
