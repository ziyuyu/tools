<?php

use Tools\Swoole\Inotify;
use Tools\Swoole\WatchEvent;

array_shift($argv);
$call = implode(' ', $argv);
$swoftPath = __DIR__ . "/swoft ";

$pid = pcntl_fork();
if ($pid == 0) {
    exec("php " . $swoftPath . $call, $result, $status);
} else {
    $call = str_replace('start', 'restart', $call);
    $inotify = new Inotify(__DIR__ . '/src/', WatchEvent::handle("php " . $swoftPath . $call));
    $inotify->start();
}