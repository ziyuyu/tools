<?php declare(strict_types = 1);

require __DIR__ . '/../../vendor/autoload.php';

\Application\Queue\Application::quickSendMessage($argv[1] ?? 'test', $argv[2] ?? 'hi');