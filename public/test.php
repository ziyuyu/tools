<?php

declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

$console = new \Tools\Command\Console();

while (true) {
    $console->write('* Content: ');
    $content = $console->input();
    $console->write("* Accept: {$content}" . PHP_EOL);
}