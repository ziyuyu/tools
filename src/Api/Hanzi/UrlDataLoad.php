<?php

declare(strict_types=1);

namespace Tools\Api\Hanzi;

/**
 * Url Storage
 *
 * Traits UrlDataLoad
 * @package Tools\Api\Hanzi
 */
trait UrlDataLoad
{
    /**
     * @var string
     */
    public $domain = 'http://10.8.7.7:8000';

    /**
     * @var array
     */
    public $urlList = [
        'initialize' => '/api/init',
        'status' => '/api/status',
    ];
}
