<?php

namespace Tools\BloomFilter;

/**
 * Interface Client
 *
 * @package App\Logic\BloomFilter
 */
interface Client
{
    /**
     * Add
     *
     * @param string $bucket
     * @param array $offsetList
     * @return bool
     */
    public function add(string $bucket, array $offsetList): bool;

    /**
     * Exists
     *
     * @param string $bucket
     * @param array $offsetList
     * @return bool
     */
    public function exists(string $bucket, array $offsetList): bool;
}
