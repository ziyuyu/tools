<?php


namespace Tools\BloomFilter;

/**
 * Class RedisClient
 *
 * @package App\Logic\BloomFilter
 */
class RedisClient implements Client
{
    /** @var \Redis $connection */
    protected $connection;

    /**
     * RedisClient constructor.
     *
     * @param \Redis $connection
     */
    public function __construct(\Redis $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Add
     *
     * @param string $bucket
     * @param array $offsetList
     * @return bool
     */
    public function add(string $bucket, array $offsetList): bool
    {
        $pipe = $this->connection->multi();
        foreach ($offsetList as $offset) {
            $pipe->setBit($bucket, $offset, 1);
        }
        return (bool) $pipe->exec();
    }

    /**
     * Exists
     *
     * @param string $bucket
     * @param array $offsetList
     * @return bool
     */
    public function exists(string $bucket, $offsetList): bool
    {
        $pipe = $this->connection->multi();
        foreach ($offsetList as $offset) {
            $pipe->getBit($bucket, $offset);
        }
        $result = $pipe->exec();

        return ! in_array(0, $result);
    }

}
