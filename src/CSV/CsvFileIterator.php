<?php

declare(strict_types=1);

namespace Tools\CSV;

/**
 * Class CsvFileIterator
 * @package Tools\CSV
 */
class CsvFileIterator implements \Iterator
{
    /**
     * @var bool|resource File stream for csv.
     */
    protected $file;

    /**
     * @var int Flag key.
     */
    protected $key = 0;

    /**
     * @var mixed Current offset contain content.
     */
    protected $current;

    public function __construct($file)
    {
        $this->file = fopen($file, 'r');
    }

    public function __destruct()
    {
        fclose($this->file);
    }

    public function rewind()
    {
        rewind($this->file);
        $this->current = fgetcsv($this->file);
        $this->key = 0;
    }

    public function valid()
    {
        return !feof($this->file);
    }

    public function key()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->current;
    }

    public function next()
    {
        $this->current = fgetcsv($this->file);
        $this->key++;
    }
}