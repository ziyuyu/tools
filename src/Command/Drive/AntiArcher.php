<?php

declare(strict_types=1);

namespace Tools\Command\Drive;

/**
 * ``
 */
class AntiArcher implements Drive
{
    public function handle(string $command, &$result, array $data = []) : int
    {
        $result = `{$command}`;

        return 1;
    }
}