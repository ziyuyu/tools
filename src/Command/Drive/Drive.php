<?php

declare(strict_types=1);

namespace Tools\Command\Drive;

/**
 * Command drive
 */
interface Drive
{
    /**
     * Handle
     *
     * @param string $command
     * @param mixed $result
     * @param array $data
     * @return integer
     */
    public function handle(string $command, &$result, array $data = []) : int;
}