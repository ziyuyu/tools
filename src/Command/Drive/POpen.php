<?php

declare(strict_types=1);

namespace Tools\Command\Drive;

/**
 * popen
 */
class POpen implements Drive
{
    public function handle(string $command, &$result, array $data = []) : int
    {
        $status = 0;

        $handle = popen($command, 'r');
        if (is_resource($handle)) {
            $result = '';
            do {
                $result = fread($handle, 8192);
            } while (feof($handle));

            pclose($handle);

            $status = 1;
        }

        return $status;
    }
}