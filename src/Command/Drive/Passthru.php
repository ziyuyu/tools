<?php

declare(strict_types=1);

namespace Tools\Command\Drive;

/**
 * passthru
 */
class Passthru implements Drive
{
    public function handle(string $command, &$result, array $data = []) : int
    {
        $status = 0;

        passthru($command, $returnVar);

        if (isset($returnVar)) $status = (int) $returnVar;

        return $status;
    }
}