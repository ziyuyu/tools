<?php

declare(strict_types=1);

namespace Tools\Command\Drive;

/**
 * shell_exec
 */
class ShellExecute implements Drive
{
    public function handle(string $command, &$result, array $data = []) : int
    {
        $result = shell_exec($command);

        return 1;
    }
}