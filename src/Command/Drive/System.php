<?php

declare(strict_types=1);

namespace Tools\Command\Drive;

/**
 * system
 */
class System implements Drive
{
    public function handle(string $command, &$result, array $data = []) : int
    {
        $status = 0;

        $lastLine = system($command, $returnVar);

        if (isset($returnVar)) $status = (int) $returnVar;

        return $status;
    }
}