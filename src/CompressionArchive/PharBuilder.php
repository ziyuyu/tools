<?php

declare(strict_types=1);

namespace Tools\CompressionArchive;

/**
 * Class PharBuilder
 *
 * @package Tools\CompressionArchive
 */
final class PharBuilder
{
    /**
     * @param $bucket
     * @param $directory
     * @param $cli
     * @param $web
     */
    public static function handle($bucket, $directory, $cli, $web)
    {
        $phar = new \Phar("./{$bucket}", \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::KEY_AS_FILENAME, $bucket);

        $phar->buildFromDirectory($directory);

        $phar->setStub($phar->createDefaultStub($cli, $web));
    }
}
