<?php

declare(strict_types=1);

namespace Tools\DataStructure;

/**
 * Class Closure
 * @package Tools\DataStructure\Collection
 */
class Closure
{
    /**
     * @var \Closure call back function
     */
    protected \Closure $closure;

    /**
     * @var array parameters
     */
    protected array $args;

    /**
     * Closure constructor.
     * @param \Closure $closure
     * @param ...$args
     */
    public function __construct(\Closure $closure, ...$args)
    {
        $this->closure = $closure;
        $this->args = $args;
    }

    /**
     * Closure execute.
     *
     * @return mixed
     */
    public function execute() : mixed
    {
        return ($this->closure)(...$this->args);
    }

    /**
     * @return \Closure
     */
    public function getClosure() : \Closure
    {
        return $this->closure;
    }

    /**
     * @param \Closure $closure
     */
    public function setClosure(\Closure $closure)
    {
        $this->closure = $closure;
    }

    /**
     * @return array
     */
    public function getArgs() : array
    {
        return $this->args;
    }

    /**
     * @param array $args
     */
    public function setArgs(array $args)
    {
        $this->args = $args;
    }
}
