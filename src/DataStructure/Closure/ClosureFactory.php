<?php

declare(strict_types = 1);

namespace Tools\DataStructure\Closure;

use JetBrains\PhpStorm\Pure;
use Tools\DataStructure\Closure;

/**
 * Class ClosureFactory
 * @package Tools\DataStructure\Closure
 */
final class ClosureFactory
{
    /** Utils */
    use ClosureUtils;

    /**
     * Create a instance.
     *
     * @param \Closure $closure
     * @param ...$parameters
     * @return Closure
     */
    #[Pure]
    public static function create(\Closure $closure, ...$parameters) : Closure
    {
        return new Closure($closure, ...$parameters);
    }
}