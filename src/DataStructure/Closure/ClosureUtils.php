<?php

declare(strict_types = 1);

namespace Tools\DataStructure\Closure;

use Tools\DataStructure\Closure;

/**
 * Class ClosureUtils
 * @package Tools\DataStructure\Closure
 */
trait ClosureUtils
{
    /**
     * Run a closure.
     *
     * @param Closure $closure
     * @return mixed
     */
    public static function execute(Closure $closure): mixed
    {
        return $closure->execute();
    }
}