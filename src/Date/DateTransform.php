<?php

declare(strict_types=1);

namespace Tools\Date;

/**
 * Class DateTransform
 * @package Tools\Date
 */
class DateTransform
{
    /**
     * Get current microtime
     */
    public static function getCurrentMicrotime()
    {
        // array:2 [▼
        //  0 => "0.12598000"
        //  1 => "1627614695"
        //]
        // $data = explode(' ', microtime());

        // $data[1] .= $data[0][2];
        // $data[1] .= $data[0][3];
        // $data[1] .= $data[0][4];

        // return (int) $data[1];

        return bcmul(microtime(true), 1000);
    }
}
