<?php

declare(strict_types = 1);

namespace Tools\DesignPattern;

/**
 * Singleton Pattern.
 * Trait Factory
 *
 * @package Tools\DesignPattern
 */
trait Factory
{
    /**
     * Create instance.
     *
     * @param string $class
     * @param array $parameters
     * @return mixed
     */
    public static function create(string $class, array $parameters = []): mixed
    {
        return new $class(...$parameters);
    }
}