<?php

declare(strict_types = 1);

namespace Tools\DesignPattern;

/**
 * Singleton Pattern.
 * Trait Singleton
 *
 * @package Tools\DesignPattern
 */
trait Singleton
{
    /**
     * @var null|Singleton singleton instance.
     */
    private static ?Singleton $instance;

    private function __construct() {}

    private function __clone() {}

    /**
     * get self instance.
     * 
     * @param mixed ...$args
     * @return Singleton
     */
    public static function getInstance(...$args) : Singleton
    {
        if (! is_null(self::$instance)) {
            self::$instance = new self(...$args);
        }

        return self::$instance;
    }
}
