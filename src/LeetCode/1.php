<?php

/*
 * @see https://leetcode.cn/problems/two-sum/
 */

class Solution
{
    function handle($input, $target)
    {
        return $this->dict($input, $target);
    }

    private function base($input, $target)
    {
        $length = count($input);

        for ($i = 0; $i < $length; $i++) {
            for ($j = 0; $j < $length; $j++) {
                if ($i !== $j && $input[$i] + $input[$j] === $target) {
                    return [$i, $j];
                }
            }
        }

        throw new \Exception();
    }

    private function dict($input, $target)
    {
        $dict = [];
        $length = count($input);

        for ($i = 0; $i < $length; $i++) {
            $current = $target - $input[$i];

            if (array_key_exists($current, $dict)) {
                return [$dict[$current], $i];
            }

            $dict[$input[$i]] = $i;
        }

        throw new \Exception();
    }
}

$data = [2,7,11,15];
$handler = new Solution();
var_dump($handler->handle($data, 13));

