<?php

declare(strict_types=1);

namespace Tools\lock;

/**
 * Interface Lock
 *
 * @package Tools\lock
 */
interface Lock
{
    /**
     * Lock
     *
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @param array $data
     * @return bool
     */
    public function lock($key, $value, $ttl, $data = []);

    /**
     * Unlock
     *
     * @return bool
     */
    public function unlock();

    /**
     * Check that you have a lock
     *
     * @return bool
     */
    public function isLock();

    /**
     * Check for the exclusive lock
     *
     * @return bool
     */
    public function monopolize();
}
