<?php

declare(strict_types = 1);

namespace Tools\Net;

/**
 * Class FormRequet
 *
 * @package Tools\Net
 */
class FormRequest
{
    /**
     * Send the request in a form way.
     *
     * @param string $url     Request address.
     * @param string $data    Refer to the $request_uri. 'app_code=*app_id=*'
     *
     * @static
     * 
     * @return false|string
     */
    public static function send(string $url, string $data) : mixed
    {
        $options = [
            'http' => [
                'method'  => 'POST',
                'header'  => '',
                'content' => $data,
                'timeout' => 15 * 60, // second
            ],
        ];

        $context = stream_context_create($options);

        $result = file_get_contents($url, true, $context);

        return $result;
    }
}