<?php

declare(strict_types=1);

namespace Tools\Net;

/**
 * Proxy Pattern
 *
 * Class HTTPSProxy
 *
 * @package Tools\Net
 */
class HTTPSProxy
{
    /**
     * @param string $url
     * @param array $data
     * @param string $type
     * @param array $options
     * @param array $headers
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public static function handle(
        string $url, array $data = [], string $type = 'GET',
        array $options = [], array $headers = [] )
    {
        HTTPS::initCurl($options, $headers);

        $result = HTTPS::sendRequest($url, $data, $type);

        HTTPS::clean();

        if ($result != '') {
            $obj = json_decode($result);
            if (!is_null($obj)) {
                return $obj;
            }
        }

        throw new \Exception('Get result failure!');
    }
}
