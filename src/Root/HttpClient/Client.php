<?php

namespace Tools\Root\HttpClient;

/**
 * Interface Client
 *
 * @package Tools\Root\HttpClient
 */
interface Client
{
    /**
     * Initialize.
     *
     * @return void
     */
    public function initialize() ;

    /**
     * Close client.
     *
     * @return void
     */
    public function close() ;

    /**
     * Service status.
     *
     * @return bool
     */
    public function status() : bool ;

    /**
     * Send request.
     *
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return \stdClass
     */
    public function request(string $method, string $uri = '', array $options = []) : \stdClass;
}

