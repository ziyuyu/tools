<?php

namespace Tools\Root\HttpClient;

/**
 * Class Factory
 *
 * @package Tools\Root\HttpClient
 */
trait Factory
{
    /**
     * Create instance.
     *
     * @param string $class
     * @param array $parameters
     * @return mixed
     */
    public static function create(string $class, array $parameters = [])
    {
        $instance = new $class(...$parameters);

        return $instance;
    }
}
