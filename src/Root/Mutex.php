<?php

// file exists /dev/shm/*


$syncMutex = new \SyncMutex($argv[1]);

if ($syncMutex->lock()) {
    echo 'lock success!' . PHP_EOL;
}

$syncMutex->unlock(true);