<?php

declare(strict_types = 1);

namespace Tools\String;

/**
 * Trait StringHandler
 *
 * @package Tools\String
 */
trait StringHandler
{
    /**
     * Hump naming Go to underscore naming.
     *
     * @param string $string
     * @param string $separator = '_'
     *
     * @static
     *
     * @return string
     */
    public static function toUnderScore(string $string, string $separator = '_') : string
    {
        // Lercase and capital are close together, add the separator and all to lowercase.
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $string));
    }

    /**
     * Underscore named to hump named.
     *
     * @param string $string
     * @param string $separator = '_'
     *
     * @static
     * 
     * @return string
     */
    public static function toCamelCase(string $string, string $separator = '_') : string
    {
        // step1: original string to lowercase,
        //      separator in the original string is replaced with space,
        //      plus separator at the beginning of the string
        // step2: converts the initials of each word in the string to uppercase,
        //      then goes to the space and the separator attached to the beginning of the string.
        $string = $separator . str_replace('_', ' ', strtolower($string));

        return ltrim(str_replace(' ', '', ucwords($string)), $separator);
    }
}
