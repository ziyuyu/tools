<?php

namespace Tools\Swoole;

class WatchEvent
{
    public static function handle($call)
    {
        return function ($event) use ($call) {
            $action = 'file:';
            switch ($event['mask']) {
                case IN_CREATE:
                    $action = 'create';
                    break;
                case IN_DELETE:
                    $action = 'delete';
                    break;
                case IN_MODIFY:
                    $action = 'modify';
                    break;
                case IN_MOVE:
                    $action = 'move';
                    break;
            }

            exec($call, $result, $status);
        };
    }
}